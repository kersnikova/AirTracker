#include <ArduinoJson.h>
#include <DHT.h>

#define DHTTYPE    DHT22 

#define DHTPIN 2 
#define BUTTON_PIN 3
DHT dht(DHTPIN, DHTTYPE);

#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

#define OLED_RESET -1
Adafruit_SSD1306 display(OLED_RESET);

#define SCREEN_WIDTH 64 // OLED display width,  in pixels
#define SCREEN_HEIGHT 48 // OLED display height, in pixels

int measurePin = A0;
float voMeasured = 0;
float calcVoltage = 0;
float dustDensity = 0;

String inputString = "";
bool transmissionStringComplete = false; 

// the setup routine runs once when you press reset:
void setup() {
  Serial.begin(9600);
  // initialize the digital pin as an output.
  pinMode(LED_BUILTIN, OUTPUT);
  pinMode(BUTTON_PIN, INPUT);
  dht.begin();

  display.begin(SSD1306_SWITCHCAPVCC, 0x3C);
  display.clearDisplay();
  display.setTextSize(1);
  display.setTextColor(WHITE);
  display.println("START");
  display.display();
}

// the loop routine runs over and over again forever:
void loop() {
  voMeasured = analogRead(measurePin);
  calcVoltage = voMeasured * (5.0 / 1024.0);
  dustDensity = 170 * calcVoltage - 0.1;


  display.clearDisplay();
  display.setTextSize(1);
  display.setCursor(0, 0);
  display.setTextColor(WHITE);

//  display.println(gps.location.lat(), 6);
//  display.println(gps.location.lng(), 6);
    
  display.print("Du:");
  display.println(dustDensity);

//  display.print("H %: ");
//  display.println(humidity, 1);
//  display.print("T C: ");
//  display.println(temperature, 1);
//  display.print("HiC: ");
//  display.println(Hindx, 1);
  
  display.display();

  if (digitalRead(BUTTON_PIN) == HIGH) {
    StaticJsonDocument<512> sporocilo;
    sporocilo["Dust density"] = dustDensity;
    sporocilo["Humidity"] = dht.readHumidity();  
    sporocilo["Temperature"] = dht.readTemperature();
    serializeJson(sporocilo,Serial);
    Serial.println();

    digitalWrite(LED_BUILTIN, HIGH);   // turn the LED on (HIGH is the voltage level)
    delay(1000);               // wait for a second  
    digitalWrite(LED_BUILTIN, LOW);    // turn the LED off by making the voltage LOW
    delay(200); 
  }
}

void serialEvent() {
  while (Serial.available()) {
    // get the new byte:
    char inChar = (char)Serial.read();
    // add it to the inputString:
    inputString += inChar;
    // if the incoming character is a newline, set a flag so the main loop can
    // do something about it:
    if (inChar == '\n') {
      transmissionStringComplete = true;
    }    
  }   
}
