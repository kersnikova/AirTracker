# AirTracker

A Track to Air  

GPS: good  
Temp: 27°C  
Hum: 40%  
Heat Indx: 28°C  
Dust: 200 ug/m3  
NH3: 100 PPM  
...  

Button 1: on -> 1 min, then sleep  
Button 2: write values to SD  

Pins

OLED I2C 0x3C  
D1  	GPIO 	Shield  
D1 	    5 	       SCL  
D2 	    4 	       SDA  
D3.               Button A  
D4     0        Button B  
Button A or B solder to D3, D4, D5, D6, D7  

Battery  
D1      Shield  
5V      Power Supply, 5V(max: 1A)  
GND  GND  

SD card  
D1  GPIO  Shield  
D5  14  CLK  
D6  12  MISO  
D7  13  MOSI  
D4  0    CS  
Configurable CS pin, Default: D4 (GPIO0)  

DHT  
D1   GPIO   Shield  
D4   0         DHT  

dust  
A0  
Dn  


https://randomnerdtutorials.com/esp8266-pinout-reference-gpios/  

Label	GPIO	Input		Output	AirT		Notes  
D0	    + 16	    + no			no 					PWM or I2C support	HIGH at boot, used to wake up from deep sleep  
D1	    5	    OK			OK		OLED		often used as SCL (I2C)  
D2	    4	    OK			OK		OLED		often used as SDA (I2C)  
D3	    0	    pulled up	OK		bA			connected to FLASH button, boot fails if pulled LOW  
D4	    2      	pulled up	OK		SD/bB/DHT	HIGH at boot connected to on-board LED, boot fails if pulled LOW  
D5		14	    OK			OK		SD			SPI (SCLK)  
D6		12		OK			OK		SD			SPI (MISO)  
D7		13		OK			OK		SD			SPI (MOSI)  
D8		15		pulledtoGND	OK					SPI (CS), Boot fails if pulled HIGH  
RX		3		OK			RX pin	GPS			HIGH at boot  
TX		1		TX pin		OK		GPS			HIGH at boot, debug output at boot, boot fails if pulled LOW  
A0		ADC0	AnalogInput	X		Dust  
