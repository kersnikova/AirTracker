#include <TinyGPSPlus.h>
#include <SoftwareSerial.h>
/*
   This sample sketch demonstrates the normal use of a TinyGPSPlus (TinyGPSPlus) object.
   It requires the use of SoftwareSerial, and assumes that you have a
   4800-baud serial GPS device hooked up on pins D7(rx) and D6(tx).
*/
static const int RXPin = D6, TXPin = D7;
static const uint32_t GPSBaud = 9600;

// The TinyGPSPlus object
TinyGPSPlus gps;

// The serial connection to the GPS device
SoftwareSerial ss(RXPin, TXPin);

#include <Wire.h>
#include <LOLIN_I2C_BUTTON.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

const int R_0 = 945; //Change this to your own R0 measurements
#define METHANE_PIN D3

#define OLED_RESET -1
Adafruit_SSD1306 display(OLED_RESET);

#include "DHTesp.h"
int dhtPin = D4;
#ifdef ESP32
#pragma message(THIS EXAMPLE IS FOR ESP8266 ONLY!)
#error Select ESP8266 board.
#endif
DHTesp dht;

int measurePin = A0; //Connect dust sensor to Arduino A0 pin
int ledPower = D5;   //Connect led driver pins of dust sensor to Arduino D2
int samplingTime = 280;
int deltaTime = 40;
int sleepTime = 9680;
float voMeasured = 0;
float calcVoltage = 0;
float dustDensity = 0;

void setup(){
  Serial.begin(115200);
  pinMode(ledPower,OUTPUT);

  ss.begin(GPSBaud);
  Serial.println(F("DeviceExample.ino"));
  Serial.println(F("A simple demonstration of TinyGPSPlus with an attached GPS module"));
  Serial.print(F("Testing TinyGPSPlus library v. "));
  Serial.println(TinyGPSPlus::libraryVersion());
  Serial.println(F("by Mikal Hart"));
  Serial.println();

  Serial.println();
  Serial.println("Status\tHumidity (%)\tTemperature (C)\t(F)\tHeatIndex (C)\t(F)");
  String thisBoard= ARDUINO_BOARD;
  Serial.println(thisBoard);
  dht.setup(dhtPin, DHTesp::DHT11);
  
  display.begin(SSD1306_SWITCHCAPVCC, 0x3C);
  display.clearDisplay();
  display.setTextSize(1);
  display.setTextColor(WHITE);
  display.println("START");
  display.display();
}

void loop(){
    // This sketch displays information every time a new sentence is correctly encoded.
  while (ss.available() > 0)
    if (gps.encode(ss.read()))
      displayInfo();

  if (millis() > 5000 && gps.charsProcessed() < 10)
  {
    Serial.println(F("No GPS detected: check wiring."));
  }
  
  digitalWrite(ledPower,LOW); // power on the LED
  delayMicroseconds(samplingTime);
  voMeasured = analogRead(measurePin); // read the dust value
  delayMicroseconds(deltaTime);
  digitalWrite(ledPower,HIGH); // turn the LED off
  delayMicroseconds(sleepTime);
  // 0 - 5V mapped to 0 - 1023 integer values
  // recover voltage
  calcVoltage = voMeasured * (5.0 / 1024.0);
  dustDensity = 170 * calcVoltage - 0.1;
  
  Serial.println(F("DUST SENSOR"));
  Serial.print(F("The dust concentration is: "));
  Serial.print(dustDensity);
  Serial.print(F(" ug/m3\n"));

  Serial.println(F("METHANE SENSOR"));
  Serial.print(F("The methane concentration is above the threshold: "));
  Serial.println(digitalRead(METHANE_PIN) ? F("Yes") : F("No"));


  float humidity = dht.getHumidity();
  float temperature = dht.getTemperature();

  Serial.println(F("DHT11"));
  Serial.print(dht.getStatusString());
  Serial.print(F("\t"));
  Serial.print(F("Hum: "));
  Serial.print(humidity, 2);
  Serial.print("%");
  Serial.print("\t");
  Serial.print("Temp: ");
  Serial.print("\t");
  Serial.print(temperature, 2);
  Serial.print("C ");
  Serial.print("\t");
  Serial.print("Hind: ");
  Serial.print("\t");
  Serial.print(dht.computeHeatIndex(temperature, humidity, false), 2);
  int Hindx = dht.computeHeatIndex(temperature, humidity, false);
  Serial.println("C ");
  Serial.println("************");
  Serial.println("");

  display.clearDisplay();
  display.setTextSize(1);
  display.setCursor(0, 0);
  display.setTextColor(WHITE);

  display.println(gps.location.lat(), 6);
  display.println(gps.location.lng(), 6);
    
  display.print(F("CH4:"));
  display.println(getMethanePPM());

  display.print(F("H %: "));
  display.println(humidity, 1);
  display.print("T C: ");
  display.println(temperature, 1);
  display.print(F("HiC: "));
  display.println(Hindx, 1);
  
  display.display();
  
  delay(2000);
}

void displayInfo()
{
  Serial.println("GPS");
  Serial.print(F("Location: ")); 
  if (gps.location.isValid())
  {
    Serial.print(gps.location.lat(), 6);
    Serial.print(F(","));
    Serial.print(gps.location.lng(), 6);
  }
  else
  {
    Serial.print(F("INVALID"));
  }

  Serial.print(F("  Date/Time: "));
  if (gps.date.isValid())
  {
    Serial.print(gps.date.month());
    Serial.print(F("/"));
    Serial.print(gps.date.day());
    Serial.print(F("/"));
    Serial.print(gps.date.year());
  }
  else
  {
    Serial.print(F("INVALID"));
  }

  Serial.print(F(" "));
  if (gps.time.isValid())
  {
    if (gps.time.hour() < 10) Serial.print(F("0"));
    Serial.print(gps.time.hour());
    Serial.print(F(":"));
    if (gps.time.minute() < 10) Serial.print(F("0"));
    Serial.print(gps.time.minute());
    Serial.print(F(":"));
    if (gps.time.second() < 10) Serial.print(F("0"));
    Serial.print(gps.time.second());
    Serial.print(F("."));
    if (gps.time.centisecond() < 10) Serial.print(F("0"));
    Serial.print(gps.time.centisecond());
  }
  else
  {
    Serial.print(F("INVALID"));
  }
  Serial.println("");
}

float getMethanePPM(){
   float a0 = analogRead(A0); // get raw reading from sensor
   float v_o = a0 * 5 / 1023; // convert reading to volts
   float R_S = (5-v_o) * 1000 / v_o; // apply formula for getting RS
   float PPM = pow(R_S/R_0,-2.95) * 1000; //apply formula for getting PPM
   return PPM; // return PPM value to calling function
}
