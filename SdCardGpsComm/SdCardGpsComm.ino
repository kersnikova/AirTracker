#include <ArduinoJson.h>
#include <SD.h>
#include <SPI.h>
#include <TinyGPSPlus.h>
#include <SoftwareSerial.h>

#define CHIP_SELECT_PIN D8

static const int RXPin = D1, TXPin = D2;
static const uint32_t GPSBaud = 9600;
TinyGPSPlus gps;

int logCounter = 0;
String inputString = "";
bool transmissionStringComplete = false; 

SoftwareSerial ss(RXPin, TXPin);
File SDFile;


void setup() {
  pinMode(LED_BUILTIN, OUTPUT);
  // put your setup code here, to run once:
  Serial.begin(9600);
  if (!SD.begin(CHIP_SELECT_PIN)){
    Serial.println(CHIP_SELECT_PIN);
    Serial.print("SD init failed");
    return;
  } else {
    Serial.print("SD init.");
  }

  ss.begin(GPSBaud);
  
  File iterTmp = SD.open("/");
  logCounter = countFiles(iterTmp);
  Serial.print(F("Na SD kartici je "));
  Serial.print(logCounter);
  Serial.println(F(" datotek."));
  iterTmp.close();  
}

void loop() {  
  if (transmissionStringComplete) {
    StaticJsonDocument<512> vsebinaDatoteke;
    deserializeJson(vsebinaDatoteke,inputString);
    logCounter++;
    vsebinaDatoteke["Število zapisa"] = logCounter;
    
    while (ss.available() > 0) {
      if (gps.encode(ss.read())) {
        if (gps.location.isValid()) {
          vsebinaDatoteke["Location"]["lat"] = gps.location.lat();
          vsebinaDatoteke["Location"]["long"] = gps.location.lng();
        }  else {
          vsebinaDatoteke["Location"]= F("INVALID");
        }
        
        if (gps.date.isValid()) {
          vsebinaDatoteke["Date"]["Day"] = gps.date.day();
          vsebinaDatoteke["Date"]["Month"] = gps.date.month();
          vsebinaDatoteke["Date"]["Year"] = gps.date.year();
        } else {
          vsebinaDatoteke["Date"]= F("INVALID");
        }      
        if (gps.time.isValid()) {
          vsebinaDatoteke["Time"]["Hour"] = gps.time.hour();
          vsebinaDatoteke["Time"]["Minute"] = gps.time.minute();
          vsebinaDatoteke["Time"]["Second"] = gps.time.second();
        } else {
          vsebinaDatoteke["Time"]= F("INVALID");
        }
      }
    }        
 
    char fileName[14];
    sprintf(fileName,"log%05d.json",logCounter);
    SDFile = SD.open(fileName,FILE_WRITE);  

    serializeJsonPretty(vsebinaDatoteke,SDFile);
    serializeJsonPretty(vsebinaDatoteke,Serial);
    digitalWrite(LED_BUILTIN, LOW); 
    delay(200);
    digitalWrite(LED_BUILTIN, HIGH);  
    delay(200);
    digitalWrite(LED_BUILTIN, LOW); 
    delay(200);
    digitalWrite(LED_BUILTIN, HIGH);  
    delay(200);
    digitalWrite(LED_BUILTIN, LOW); 
    delay(200);
    digitalWrite(LED_BUILTIN, HIGH);  
    SDFile.close();    
    transmissionStringComplete = false;
    
//  File iterTmp = SD.open("/");
//  printDirectory(iterTmp,0);
//  iterTmp.close(); 
  }

}
/*
void printDirectory(File dir, int numTabs) {
   while(true) {
     File entry =  dir.openNextFile();
     if (! entry) {
       // no more files
       break;
     }
     for (uint8_t i=0; i<numTabs; i++) {
       Serial.print('\t');
     }
     Serial.print(entry.name());
     if (entry.isDirectory()) {
       Serial.println("/");
       printDirectory(entry, numTabs+1);
     } else {
       // files have sizes, directories do not
       Serial.print("\t\t");
       Serial.println(entry.size(), DEC);
     }
     entry.close();
   }
}
*/
int countFiles(File dir) {
  int fileCount = 0;
  while(true) {
    File entry =  dir.openNextFile();
    if (! entry) {
      // no more files
      break;
    }
    if (!entry.isDirectory()) {
      fileCount++;  
    }
    entry.close();
  }
  return fileCount;
}

void serialEvent() {
  while (Serial.available()) {
    // get the new byte:
    char inChar = (char)Serial.read();
    // add it to the inputString:
    inputString += inChar;
    // if the incoming character is a newline, set a flag so the main loop can
    // do something about it:
    if (inChar == '\n') {
      transmissionStringComplete = true;
    }    
  }  
  if (transmissionStringComplete) {
    digitalWrite(LED_BUILTIN, LOW); 
    delay(200);
    digitalWrite(LED_BUILTIN, HIGH);  
    delay(1000);
  }
}
